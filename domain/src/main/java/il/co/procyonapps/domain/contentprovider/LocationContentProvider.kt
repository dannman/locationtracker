package il.co.procyonapps.domain.contentprovider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.hardware.TriggerEvent
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.core.net.UriCompat
import androidx.sqlite.db.SimpleSQLiteQuery
import il.co.procyonapps.contract.LocationContract.*
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.domain.R
import il.co.procyonapps.domain.room.LocationEntity
import il.co.procyonapps.domain.room.LocationsDbImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi


class LocationContentProvider : ContentProvider() {



    private val db: LocationsDbImpl by lazy { LocationsDbImpl }

    lateinit var uriMatcher: UriMatcher

    override fun onCreate(): Boolean {

        uriMatcher = UriMatcher(UriMatcher.NO_MATCH).apply {
            addURI(AUTHORITY, PATH, 1)
            addURI(AUTHORITY, "$PATH/#", 2)
        }

        return true
    }

    @ExperimentalCoroutinesApi
    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {

        return when (uriMatcher.match(uri)) {

            1 -> {
                val projString = projection.toSqlArgument()
                val selectionString = if (selection != null) "WHERE $selection" else ""
                val simpleSQLiteQuery = SimpleSQLiteQuery(
                    "SELECT $projString FROM locations $selectionString ORDER BY id ${sortOrder ?: "asc"}",
                    selectionArgs
                )
                db.readAllByRawQuery(simpleSQLiteQuery)
            }

            2 -> {
                val idToQuery = uri.lastPathSegment?.toInt()
                    ?: throw IllegalArgumentException("path arg must be Int")
                db.getSingleLocationById(idToQuery)
            }
            else -> null
        }

    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {


        val uriId = uri.idOrNull()
        val loc = values.toLocationEntity(uriId)
        return if (loc != null) {
            val inserted = db.insertFromCP(loc)
            Uri.parse("$AUTHORITY/$PATH/$inserted")
        } else null

    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        val uriId = uri.idOrNull()
        val loc = values.toLocationEntity(uriId)
        return if (loc != null) db.updateFromCP(loc) else 0
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        val id = uri.idOrNull()
        return if (id != null) {
            db.deleteFromCP(id)
        } else 0
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    private fun Uri.idOrNull(): Int? {
        return when (uriMatcher.match(this)) {
            2 -> this.lastPathSegment?.toInt()
            else -> null
        }
    }

    private fun ContentValues?.toLocationEntity(lineId: Int? = null): LocationEntity? {
        if (this == null) return null

        val id = when {
            lineId != null -> lineId
            containsKey(ID) -> getAsInteger(ID)
            else -> 0
        }
        val lat = if (containsKey(LAT)) getAsDouble(LAT) else Double.NaN
        val lng = if (containsKey(LNG)) getAsDouble(LNG) else Double.NaN
        val timestamp = if (containsKey(TIMESTAMP)) getAsString(TIMESTAMP) else ""
        val trigger =
            if (containsKey(TRIGGER)) LocationEvent.EventTrigger.valueOf(getAsString(TRIGGER)) else LocationEvent.EventTrigger.NONE
        val type =
            if (containsKey(MOTION_TYPE)) LocationEvent.MotionType.valueOf(getAsString(MOTION_TYPE)) else LocationEvent.MotionType.NONE
        val synced = if (containsKey(SYNCED)) getAsBoolean(SYNCED) else false

        return LocationEntity(id, lat, lng, timestamp, trigger, type, synced)
    }

    private fun Array<out String>?.toSqlArgument(defVal: String = "*"): String {
        if (this == null || this.isEmpty()) return defVal

        val builder = StringBuilder()

        this.forEachIndexed { index: Int, s: String ->
            if (index == this.size - 1) {
                builder.append(s)
            } else {
                builder.append("$s, ")
            }
        }
        return builder.toString()
    }
}