package il.co.procyonapps.domain.room

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.Room
import androidx.sqlite.db.SimpleSQLiteQuery
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.usecases.LocationUsecase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDate

object LocationsDbImpl : LocationUsecase {

    private val db = Room.databaseBuilder(
            ContextProvider.appContext!!,
            LocationsDatabase::class.java,
            "locations-db"
        )
        .build()


    private val locationsDao = db.locationsDao()

    override suspend fun logRegisteredLocation(event: LocationEvent) = withContext(Dispatchers.IO) {
        locationsDao.insertLocation(LocationEntity.mapFrom(event))
    }

    override suspend fun readAllLocations(): List<LocationEvent> {
        return locationsDao.getAllLocations().map { LocationEntity.mapTo(it) }
    }

    override suspend fun readLocationsForDate(date: LocalDate): List<LocationEvent> {
        return locationsDao.getLocationsForDate(date.toString()).map { LocationEntity.mapTo(it) }
    }

    fun readLocationsForDateLiveData(date: LocalDate): LiveData<List<LocationEvent>> {
        val locationsForDateLiveData = locationsDao.getLocationsForDateLiveData(date.toString())
        return Transformations.map(locationsForDateLiveData){it ->
            return@map it.map { loc -> LocationEntity.mapTo(loc) }
        }

    }

    override suspend fun readLocationsNotSynced(): List<LocationEvent> {
        return locationsDao.getAllNonSynced().map { LocationEntity.mapTo(it) }
    }

    override suspend fun updateSyncedLocations(vararg event: LocationEvent) {
        val updates = event.map { locationEvent: LocationEvent ->
            LocationEntity.mapFrom(locationEvent).also { it.hasBeenSynced = true }
        }
        locationsDao.updateLocations(*updates.toTypedArray())
    }

    override suspend fun deleteAll() {
        locationsDao.deleteAll()
    }

    override suspend fun deleteLocation(vararg event: LocationEvent) {
        locationsDao.delete(*event.map { LocationEntity.mapFrom(it) }.toTypedArray())
    }




    //region provider functions
    fun readAllByRawQuery(query: SimpleSQLiteQuery): Cursor{
        return locationsDao.rawQueryGetAllLocations(query)
    }

    fun getSingleLocationById(id: Int): Cursor{
        return locationsDao.getSingleLocation(id)
    }

    fun insertFromCP(loc: LocationEntity): Int{
        return locationsDao.insertLocationRaw(loc).toInt()
    }

    fun deleteFromCP(locId: Int): Int{
        return locationsDao.deleteLocationRaw(locId)
    }

    fun updateFromCP(loc: LocationEntity): Int{
        return locationsDao.updateLocationRaw(loc)
    }
    //endregion


}