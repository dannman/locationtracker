package il.co.procyonapps.domain.room

import androidx.room.TypeConverter
import il.co.procyonapps.data.LocationEvent

class Converters {
    @TypeConverter
    fun fromEventTrigger(trigger: LocationEvent.EventTrigger): String {
        return trigger.name
    }

    @TypeConverter
    fun toEventTrigger(name: String): LocationEvent.EventTrigger {
        return LocationEvent.EventTrigger.valueOf(name)
    }

    @TypeConverter
    fun fromMotionType(motion: LocationEvent.MotionType): String {
        return motion.name
    }

    @TypeConverter
    fun toMotionType(name: String): LocationEvent.MotionType {
        return LocationEvent.MotionType.valueOf(name)
    }
}