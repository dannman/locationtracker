package il.co.procyonapps.domain.room

import androidx.room.*
import il.co.procyonapps.data.LocationEvent
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.xml.validation.Schema

@Entity(tableName = "locations")
@TypeConverters(Converters::class)
data class LocationEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "latitude") val lat: Double,
    @ColumnInfo(name = "longitude") val lng: Double,
    @ColumnInfo(name = "timestamp") val timeStamp: String,
    @ColumnInfo(name = "event_trigger") val eventTrigger: LocationEvent.EventTrigger,
    @ColumnInfo(name = "motion_type") val motionType: LocationEvent.MotionType,
    @ColumnInfo(name = "sync_state") var hasBeenSynced: Boolean

) {
    companion object {
        fun mapFrom(schema: LocationEvent) = LocationEntity(
            id = schema.id,
            lat= schema.lat,
            lng = schema.lng,
            timeStamp = schema.timestamp.toString(),
            eventTrigger = schema.trigger,
            motionType = schema.motionType,
            hasBeenSynced = false

        )

        fun mapTo(schema: LocationEntity): LocationEvent = LocationEvent(
            id = schema.id,
            lat = schema.lat,
            lng = schema.lng,
            timestamp = LocalDateTime.parse(schema.timeStamp),
            motionType = schema.motionType,
            trigger = schema.eventTrigger

        )
    }
}
