package il.co.procyonapps.domain.room

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery

@Dao
interface LocationDao {

    @Insert
    suspend fun insertLocation(loc: LocationEntity)

    @Query("SELECT * FROM locations")
    suspend fun getAllLocations(): List<LocationEntity>

    @Query("SELECT * FROM locations WHERE date(:date) LIKE date(timestamp)")
    suspend fun getLocationsForDate(date: String): List<LocationEntity>

    @Query("SELECT * FROM locations WHERE date(:date) LIKE date(timestamp)")
    fun getLocationsForDateLiveData(date: String): LiveData<List<LocationEntity>>

    @Query("SELECT * FROM locations WHERE sync_state LIKE '0'")
    suspend fun getAllNonSynced(): List<LocationEntity>

    @Update
    suspend fun updateLocations(vararg location: LocationEntity)

    @Query("DELETE  FROM locations")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(vararg loc: LocationEntity)



    //region Content provider calls
    @Query("SELECT :columns FROM locations")
    fun getLocationsCursor(vararg columns: String ): Cursor

    @RawQuery(observedEntities = [LocationEntity::class])
     fun rawQueryGetAllLocations(query: SimpleSQLiteQuery): Cursor

    @Query("SELECT * FROM locations WHERE id LIKE :id")
     fun getSingleLocation(id: Int): Cursor

    @Insert
    fun insertLocationRaw(loc: LocationEntity): Long

    @Query("DELETE  FROM locations WHERE id = :locId")
    fun deleteLocationRaw(locId: Int): Int

    @Update
    fun updateLocationRaw(loc: LocationEntity): Int
    //endregion

}