package il.co.procyonapps.domain.location

import android.app.Notification
import android.app.NotificationChannel
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.domain.R
import il.co.procyonapps.domain.room.LocationsDbImpl
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneOffset

class LocationTrackingService : Service() {

    lateinit var locationService: FusedLocationProviderClient

    val REQUEST_CODE = 1001
    val FOREGROUND_ID = 1002

    val looper = Looper.myLooper()

    val locationsDb by lazy { LocationsDbImpl }

    override fun onCreate() {
        super.onCreate()
        Log.d("LocationTrackingService", "onCreate")
        locationService = LocationServices.getFusedLocationProviderClient(this)
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("LocationTrackingService", "onStartCommand")
        registerAsForegroundService()
        startRecordingLocations()
        return START_STICKY
    }


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    override fun onDestroy() {
        super.onDestroy()
        stopRecordingLocations()
    }

    private fun startRecordingLocations() {
        Log.d("LocationTrackingService", "start recording")
        locationService.lastLocation.addOnSuccessListener {
            MainScope().launch {
                val event = mapToLocationEvent(it)

                Log.d("LocationTrackingService", "initial 'last know location'= $event")
                locationsDb.logRegisteredLocation(event)
            }
        }

        val locRerquest = LocationRequest().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            smallestDisplacement = 50.0f
            interval = 10000
        }


        locationService.requestLocationUpdates(
            locRerquest,
            locationCallback, null
        )
    }



    private val locationCallback = object : LocationCallback() {


        override fun onLocationResult(location: LocationResult?) {
            MainScope().launch {
                location?.let {
                    val event = mapToLocationEvent(it.lastLocation)
                    Log.d("LocationTrackingService", "location callback: new location recorded= $event")
                    locationsDb.logRegisteredLocation(event)
                }
            }

        }


    }

    private fun stopRecordingLocations(){
        Log.d("LocationTrackingService", "stop recording")
        locationService.removeLocationUpdates(locationCallback)
    }

    private fun registerAsForegroundService(){
        val pendingIntent = Intent(this, ContextProvider.intentTarget)
            .let {
                PendingIntent.getService(this, REQUEST_CODE, it, 0)
            }

        val notification = NotificationCompat.Builder(this, ContextProvider.notificationChannelID?:"")
            .setContentTitle("Location POC running")
            .setContentText("recording locations live...")
            .setContentIntent(pendingIntent)
            .setTicker("recording locations")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSmallIcon(R.drawable.ic_directions_small)
            .build()

        startForeground(FOREGROUND_ID, notification)
    }



    private fun mapToLocationEvent(it: Location): LocationEvent {
        return LocationEvent(
            lat = it.latitude,
            lng = it.longitude,
            timestamp = LocalDateTime.now(),
            trigger = LocationEvent.EventTrigger.TIMERFENCE,
            motionType = LocationEvent.MotionType.NONE
        )

    }
}