import android.app.Activity
import android.content.Context

object ContextProvider {

    var appContext: Context? = null
    var notificationChannelID: String? = null

    var intentTarget: Class<out Activity>? = null
}