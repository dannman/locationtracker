package il.co.procyonapps.domain.room

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LocationsBdImplTest {

    lateinit var db: LocationsDbImpl

    @Before
    fun setUp() {
        db = LocationsDbImpl
    }

    @After
    fun tearDown() {
        MainScope().launch {
            db.deleteAll()
        }
    }

    @Test
    fun logRegisteredLocation() {
        val size = 10


        MainScope().launch {
            val writeLocations =
                generateLocations(size)
            writeLocations.forEach {
                db.logRegisteredLocation(it)
            }

            val readLocations = db.readAllLocations()
            assertArrayEquals(readLocations.toTypedArray(), writeLocations.toTypedArray())
        }
    }

//    @Test
//    fun readAllLocations() {
//    }
//
//    @Test
//    fun readLocationsForDate() {
//    }


}