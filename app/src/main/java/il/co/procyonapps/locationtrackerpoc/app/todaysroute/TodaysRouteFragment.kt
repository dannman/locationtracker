package il.co.procyonapps.locationtrackerpoc.app.todaysroute

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.system.Os.remove
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startForegroundService
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.domain.location.LocationTrackingService
import il.co.procyonapps.locationtrackerpoc.R
import toPx

class TodaysRouteFragment : Fragment() {

    private val REQUEST_PERMISSION = 101
    private var googleMap: GoogleMap? = null
    private var polyLine: Polyline? = null

    private val viewModel: TodaysRouteViewModel by lazy {
        ViewModelProvider(this).get(
            TodaysRouteViewModel::class.java
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todays_route, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (childFragmentManager.findFragmentById(R.id.map_frag) as SupportMapFragment?)?.getMapAsync {
            googleMap = it
            Log.d("TodaysRoutFragment", "google maps set")
        }
        initiateLocationService()

        viewModel.getTodaysLocations().observe(viewLifecycleOwner, locationsObserver)

    }

    private val locationsObserver = Observer<List<LocationEvent>> {

        Log.d("TodaysRoutFragment", "new route update. size = ${it.size}")
        if(it.isNotEmpty() && googleMap != null )  {
            val latLng = it.map { loc -> LatLng(loc.lat, loc.lng) }.toTypedArray()

            polyLine?.let {poly ->
                poly.remove()
            }

            polyLine = googleMap?.addPolyline(
                PolylineOptions()
                    .add(*latLng)
                    .color(Color.RED)
                    .width(5.0f)

            )



            val bounds = LatLngBounds.builder().also { bounds ->
                for (i in latLng.indices) {
                    bounds.include(latLng[i])
                }
            }.build()

            val cu = CameraUpdateFactory.newLatLngBounds(bounds, 300.toPx, 400.toPx, 20)

            googleMap?.animateCamera(cu)
        }


    }

    private fun initiateLocationService() {
        Log.d("TestActivity", "start location service")
        if (hasFineLocationPermission && hasForegroundPermission) {
            Log.d("TestActivity", "start location service. has permission")
            startForegroundService(context!!, Intent(context, LocationTrackingService::class.java))
        } else {
            Log.d("TestActivity", "request permission")
            requestPermissions(
                arrayOf(
                    Manifest.permission.FOREGROUND_SERVICE,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), REQUEST_PERMISSION
            )
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                Log.d("TestActivity", "permissions granted")
                initiateLocationService()
            } else {
                Log.d("TestActivity", "permissions denied")
                Toast.makeText(context, "permissions not granted", Toast.LENGTH_SHORT).show()
            }
        }
    }

    val hasForegroundPermission: Boolean
        get() = hasPermission(Manifest.permission.FOREGROUND_SERVICE)

    val hasFineLocationPermission: Boolean
        get() = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    private fun hasPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context!!,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }
}