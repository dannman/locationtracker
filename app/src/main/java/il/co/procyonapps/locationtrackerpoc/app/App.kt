package il.co.procyonapps.locationtrackerpoc.app

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

class App : Application() {

    companion object {
        lateinit var appContext: Context
        const val CHANNEL_ID = "LOCATIONS_CHANNEL"
    }



    override fun onCreate() {
        super.onCreate()
        appContext = this
        createNotificationChannel()

        ContextProvider.apply {
            appContext = this@App
            intentTarget = MainActivity::class.java
            notificationChannelID = CHANNEL_ID
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, "Locations", importance).apply {
                description = "location trackers"
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}