package il.co.procyonapps.locationtrackerpoc.mock

import il.co.procyonapps.data.LocationEvent
import java.time.LocalDateTime

fun generateLocations(size: Int = 10): List<LocationEvent> {
    val list = mutableListOf<LocationEvent>()
    for (i in 0..size) {
        val motionType = when (i % 3) {
            0 -> LocationEvent.MotionType.DRIVING
            1 -> LocationEvent.MotionType.WALKING
            else -> LocationEvent.MotionType.NONE
        }

        val trigger =
            if (i % 2 == 0) LocationEvent.EventTrigger.GEOFENCE else LocationEvent.EventTrigger.TIMERFENCE

        val timestamp = LocalDateTime.of(2020, 2, 15, 12, i % 60)
        val event = LocationEvent(lat = 0.1 * i,lng =  0.2 * i,timestamp =  timestamp,trigger =  trigger,motionType =  motionType)
        list.add(event)

    }

    return list
}