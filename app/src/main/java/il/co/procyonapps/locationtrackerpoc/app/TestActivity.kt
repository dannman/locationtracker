package il.co.procyonapps.locationtrackerpoc.app

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.domain.location.LocationTrackingService
import il.co.procyonapps.domain.room.LocationsDbImpl
import il.co.procyonapps.locationtrackerpoc.R
import il.co.procyonapps.locationtrackerpoc.mock.generateLocations
import il.co.procyonapps.prettyPrint
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.Month

class TestActivity : AppCompatActivity() {


    val REQUEST_PERMISSION = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_activity)

//        testDb()

        testLocationService()

    }

    private fun testLocationService() {
        Log.d("TestActivity", "start location service")
        if (hasFineLocationPermission && hasForegroundPermission) {
            Log.d("TestActivity", "start location service. has permission")
            startForegroundService(Intent(this, LocationTrackingService::class.java))
        } else {
            Log.d("TestActivity", "request permission")
            requestPermissions(
                arrayOf(
                    Manifest.permission.FOREGROUND_SERVICE,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), REQUEST_PERMISSION
            )
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                Log.d("TestActivity", "permissions granted")
                testLocationService()
            } else {
                Log.d("TestActivity", "permissions denied")
                Toast.makeText(this, "permissions not granted", Toast.LENGTH_SHORT).show()
            }
        }
    }

    val hasForegroundPermission: Boolean
        get() = hasPermission(Manifest.permission.FOREGROUND_SERVICE)

    val hasFineLocationPermission: Boolean
        get() = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    fun hasPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun testDb() {
        val repo = LocationsDbImpl

        val generatedLocations = generateLocations()
        MainScope().launch {
            generatedLocations.forEach { repo.logRegisteredLocation(it) }

            Log.d(
                "test db",
                "original locations : ${generatedLocations.prettyPrint<List<LocationEvent>>()}"
            )

            val readLocaions = repo.readAllLocations()

            Log.d(
                "test db",
                "locations from db: ${readLocaions.prettyPrint<List<LocationEvent>>()}"
            )

            repo.deleteLocation(*readLocaions.slice(0..2).toTypedArray())

            val readLocationsAfterDelete = repo.readAllLocations()

            Log.d(
                "test db",
                "locations from db after deleting 0..2: ${readLocationsAfterDelete.prettyPrint<List<LocationEvent>>()}"
            )

            val locationsOnInvalidDay = repo.readLocationsForDate(LocalDate.of(2020, Month.MAY, 1))
            Log.d(
                "test db",
                "locations in invalid day (expected = 0): ${locationsOnInvalidDay.size}"
            )

            val locationsOnValidDay =
                repo.readLocationsForDate(LocalDate.of(2020, Month.FEBRUARY, 15))
            Log.d("test db", "locations in invalid day (expected = 8): ${locationsOnValidDay.size}")

            val locsNotSynced = repo.readLocationsNotSynced()

            Log.d("test db", "locations not synced initialy (expected = 8): ${locsNotSynced.size}")

            repo.updateSyncedLocations(*locsNotSynced.toTypedArray())

            val locsNotSyncedAfterUpdate = repo.readLocationsNotSynced()
            Log.d(
                "test db",
                "locations not synced after update (expected = 0): ${locsNotSyncedAfterUpdate.size}"
            )


            repo.deleteAll()


        }
    }
}