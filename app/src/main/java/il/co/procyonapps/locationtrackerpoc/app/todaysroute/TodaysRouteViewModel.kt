package il.co.procyonapps.locationtrackerpoc.app.todaysroute

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import il.co.procyonapps.data.LocationEvent
import il.co.procyonapps.domain.room.LocationsDbImpl
import java.time.LocalDate

class TodaysRouteViewModel : ViewModel() {

    val locationDb by lazy { LocationsDbImpl }

    fun getTodaysLocations(): LiveData<List<LocationEvent>> {
        val date = LocalDate.now()
        Log.d("TodaysRouteViewModel", "get locations for day = $date")
        return locationDb.readLocationsForDateLiveData(date)
    }
}