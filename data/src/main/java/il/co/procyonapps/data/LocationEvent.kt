package il.co.procyonapps.data

import java.time.LocalDateTime

data class LocationEvent(
    val id: Int = 0,
    val lat: Double,
    val lng: Double,
    val timestamp: LocalDateTime,
    val trigger: EventTrigger,
    val motionType: MotionType
) {


    enum class EventTrigger {
        GEOFENCE, TIMERFENCE, NONE
    }

    enum class MotionType { WALKING, DRIVING, NONE }
}
