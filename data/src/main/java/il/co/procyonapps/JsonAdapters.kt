package il.co.procyonapps

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.LocalDateTime

class JsonAdapters {

    @ToJson
   fun timeDateToJson(time: LocalDateTime): String{
       return time.toString()
   }

    @FromJson
    fun timeDateFromJson(timeString: String): LocalDateTime{
        return LocalDateTime.parse(timeString)
    }
}