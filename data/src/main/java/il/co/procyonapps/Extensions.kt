package il.co.procyonapps

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

inline fun <reified T> Any.prettyPrint(): String where T : Any{
    val moshi = Moshi.Builder()
        .add(JsonAdapters())
        .add(KotlinJsonAdapterFactory())
        .build()
    val adapter = moshi.adapter(T::class.java).indent("  ")
    return adapter.toJson(this as T)
}