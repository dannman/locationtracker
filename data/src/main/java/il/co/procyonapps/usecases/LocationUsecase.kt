package il.co.procyonapps.usecases

import il.co.procyonapps.data.LocationEvent
import java.time.*

interface LocationUsecase {

    suspend fun logRegisteredLocation(event: LocationEvent)

    suspend fun readAllLocations(): List<LocationEvent>

    suspend fun readLocationsForDate(date: LocalDate): List<LocationEvent>

    suspend fun readLocationsNotSynced(): List<LocationEvent>

    suspend fun updateSyncedLocations(vararg event: LocationEvent)

    suspend fun deleteAll()

    suspend fun deleteLocation(vararg event: LocationEvent)

}