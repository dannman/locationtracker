package il.co.procyonapps.contract;



public class LocationContract {

    final public static String ID = "id";
    final public static String LAT = "latitude";
    final public static String LNG = "longitude";
    final public static String TIMESTAMP = "timestamp";
    final public static String TRIGGER = "event_trigger";
    final public static String MOTION_TYPE = "motion_type";
    final public static String SYNCED = "sync_state";

    final public static String AUTHORITY = "il.co.procyonapps.locationtrackerpoc.LocationContentProvider";
    final public static String PATH = "locations";

}
